const express = require('express');
const app = express();
const server = require('http').Server(app);
const path = require('path');
const port = 4242;
const img = process.argv[2];
const width = process.argv[3];
const height = process.argv[4];
const io = require("socket.io")(port);

let reqClient = path.join(__dirname, '..', "pixel_client")
let reqPath = path.join(__dirname, '..', "pixel_client", "index.html");

app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost:8080");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(express.static(reqPath));
server.listen(8080);

app.get('/', function (req, res) {
    res.sendFile(reqPath);
});

var getRandomColor = function () {
    let letters = '0123456789ABCDEF';
    let color = '#';
    for (let i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
};

pixels = [];
pixelsHidden = [];

const widthMap = 800;
const heightMap = 800;

var squareW = (widthMap / width);
var squareY = (heightMap / height);

for (var x = 0; x < widthMap; x += squareW) {
    for (var y = 0; y < heightMap; y += squareY) {
        pixels.push({
            x,
            y,
            width: squareW,
            height: squareY,
            color: getRandomColor()
        });
    }
}

if (process.argv.length >= 5) {

    io.on("connection", socket => {
        console.log("User connected");
        console.log(img, width, height);
        socket.emit("canvasData", { img, width, height });

        var randUser = Math.floor(Math.random() * 1000);
        socket.username = 'Guest_' + randUser;

        socket.on('hidden', function (data) {
            pixelsHidden.push(pixels.filter((elem) =>
                elem.x <= data.x
                && data.x <= elem.x + elem.width
                && elem.y <= data.y
                && data.y <= elem.y + elem.height
            ));

            socket.emit('init', {
                img,
                pixels,
                pixelsHidden
            });
        });
        socket.emit('init', {
            img,
            pixels,
            pixelsHidden
        });
    });
}
else {
    console.error("Missing parementers: npm start -- <height> <width> <imgUrl>");
    process.exit(1);
}


process.on('uncaughtException', () => {
    process.exit(1)
})